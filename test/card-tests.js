import {assert} from 'chai';
import Card from '../common/models/Card'
import {getSuitSpec,getRankSpec,getCardSpec} from '../common/models/Card'

describe('card', function () {
  it('get specs', function () {
    var card = new Card({ rank: 6, suit: '♣' })

    var suitSpec = getSuitSpec(card)
    assert.equal(suitSpec.name, 'CLUBS')
    assert.equal(suitSpec.color, 'BLACK')

    var rankSpec = getRankSpec(card)
    assert.equal(rankSpec.name, 'SIX')
    assert.equal(rankSpec.isFace, false)

    var fullSpec = getCardSpec(card)
    assert.equal(fullSpec.rank, 'SIX')
    assert.equal(fullSpec.suit, 'CLUBS')
    assert.equal(fullSpec.color, 'BLACK')
    assert.equal(fullSpec.isFace, false)
  });
  it('get specs 2', function () {
    var card = new Card({ rank: 12, suit: '♥' })

    var suitSpec = getSuitSpec(card)
    assert.equal(suitSpec.name, 'HEARTS')
    assert.equal(suitSpec.color, 'RED')

    var rankSpec = getRankSpec(card)
    assert.equal(rankSpec.name, 'QUEEN')
    assert.equal(rankSpec.isFace, true)

    var fullSpec = getCardSpec(card)
    assert.equal(fullSpec.rank, 'QUEEN')
    assert.equal(fullSpec.suit, 'HEARTS')
    assert.equal(fullSpec.color, 'RED')
    assert.equal(fullSpec.isFace, true)
  });
});


