import {assert} from 'chai';
import {makePack, draw, deal, whoAttacks, canAttackWith, canDefenceWith, getAttackingOrder, reorderAttackingQueue } from '../common/core'
import {Card, newCard} from '../common/models/Card'
import {CardPair, newCardPair} from '../common/models/CardPair'
import _ from 'lodash'
import {createCard} from './helper'
import {List, Immutable} from 'immutable'

describe('immutable core', function () {

  describe('make pack', function () {
    it('creates pack with all shuffled cards', function () {
      var res = makePack();

      assert.equal(res.length, 36)

      // no need to test shuffled
    });

  });


  describe('getAttackingOrder', function () {

    it('skips defencing', function () {
      var res = getAttackingOrder(4, 1)
      assert.deepEqual(res, [0, 2, 3])
    });
    it('skips defencing 2', function () {
      var res = getAttackingOrder(4, 0)
      assert.deepEqual(res, [3, 1, 2])
    });
    it('skips defencing 3', function () {
      var res = getAttackingOrder(2, 0)
      assert.deepEqual(res, [1])
    });
    it('do not adds players after stopAt player', function () {
      var res = getAttackingOrder(4, 1, 3)
      assert.deepEqual(res, [0, 2])
    });
    it('do not adds players after stopAt player 2', function () {
      var res = getAttackingOrder(4, 3, 0)
      assert.deepEqual(res, [2])
    });
    it('for single player is empty array', function () {
      var res = getAttackingOrder(1, 0)
      assert.deepEqual(res, [])
    });
    it('start from passed attackerIndex', function () {
      var res = getAttackingOrder(4, 0, undefined, 1)
      assert.deepEqual(res, [1, 2, 3])
    });
  });

  describe('reorderAttackingQueue', function () {
    it('starts from AttackerIndex', function () {
      var res = reorderAttackingQueue([2, 4, 1], null, 1)
      assert.deepEqual(res, [1, 2, 4])
    });
    it('starts from AttackerIndex excluding item', function () {
      var res = reorderAttackingQueue([2, 4, 1], 2, 1)
      assert.deepEqual(res, [1, 4])
    });
    it('starts from AttackerIndex excluding AttackerIndex', function () {
      var res = reorderAttackingQueue([2, 4, 1], 2, 2)
      assert.deepEqual(res, [4, 1])
    });
    it('starts from same excluding item', function () {
      var res = reorderAttackingQueue([2, 4, 1], 1, 2)
      assert.deepEqual(res, [2, 4])
    });
    it('starts from same in single item array excluding not existing', function () {
      var res = reorderAttackingQueue([2], 1, 2)
      assert.deepEqual(res, [2])
    });
  });

  describe('draw', function () {

    it('calls callback right times', function () {
      var calls = { 0: 0, 1: 0, 2: 0 }
      draw([4, 4, 6], 1, 6, i => {
        calls[i]++
        return true
      })

      assert.deepEqual(calls, { 0: 2, 1: 2, 2: 0 })
    });

    it('stops after callback returned false', function () {
      var calls = 0

      draw([4, 4, 6], 1, 6, i => {
        calls++
        return calls <= 2
      })

      assert.deepEqual(calls, 3)
    });

    it('stops after all hands should be filled', function () {
      var calls = 0

      draw([4, 4, 6], 1, 6, i => {
        calls++
        return true
      })

      assert.deepEqual(calls, 4)
    });

    it('calls callback with all indexes while hands size less than goal', function () {
      var calls = { 0: 0, 1: 0, 2: 0 }
      draw([1, 4, 3], 0, 6, i => {
        calls[i]++
        return true
      })

      assert.deepEqual(calls, { 0: 5, 1: 2, 2: 3 })
    });

    it('calls callback starting from attackerIndex', function () {
      var calls = ""
      var order = 1
      draw([1, 4, 3], 0, 6, i => {
        calls += i
        return true
      })

      assert.equal(calls, "2221100000")
    });
  });


  describe('deal', function () {
    it('spread by 6 cards from pack to players piles', function () {
      var pack = makePack();
      var packSize = pack.length

      var res = deal(pack, 2)

      assert.equal(res.length, 2)
      assert.isOk(_.every(res, e => e instanceof Array))
      assert.equal(res[0].length, 6)
      assert.equal(res[1].length, 6)
      assert.equal(pack.length + 12, packSize)

    });

  });

  describe('whoAttacks', function () {
    it('returns number of pile with lower trump', function () {
      var pile1 = [new Card({ rank: 7, suit: '♥' }), new Card({ rank: 10, suit: '♦' })]
      var pile2 = [new Card({ rank: 8, suit: '♥' }), new Card({ rank: 6, suit: '♦' })]

      var res = whoAttacks([pile1, pile2], '♥')

      assert.equal(res, 0)
    });

    it('returns number of pile with lower trump when card repeated', function () {
      var pile1 = [new Card({ rank: 7, suit: '♥' }), new Card({ rank: 10, suit: '♦' }), new Card({ rank: 7, suit: '♥' })]
      var pile2 = [new Card({ rank: 8, suit: '♥' }), new Card({ rank: 8, suit: '♥' })]

      var res = whoAttacks([pile1, pile2], '♥')

      assert.equal(res, 0)
    });

    it('returns number of pile with lower trump when single pile', function () {
      var pile1 = [new Card({ rank: 7, suit: '♥' })]

      var res = whoAttacks([pile1], '♥')

      assert.equal(res, 0)
    });

    it('returns undefined when no pile with trumps', function () {
      var pile1 = [new Card({ rank: 7, suit: '♥' }), new Card({ rank: 10, suit: '♦' })]
      var pile2 = [new Card({ rank: 8, suit: '♥' }), new Card({ rank: 6, suit: '♦' })]

      var res = whoAttacks([pile1, pile2], '♣')

      assert.equal(res, undefined)
    });
  });


  describe('canAttackWith', function () {

    it('any card when board empty', function () {
      var board = []
      var card = createCard()
      var res = canAttackWith(board, card)

      assert.equal(res, true)
    });

    it('false when board has 6 attacking card', function () {
      var board = []
      for (var i = 0; i < 6; i++) {
        board.push(newCardPair(createCard()))
      }
      var res = canAttackWith(board, board[0])

      assert.equal(res, false)
    });

    describe('cards with same rank as on board', function () {
      it('1', function () {
        var board = [new CardPair({ attack: newCard(6, '♥') })]
        var card = new Card({ rank: 6, suit: '♣' })
        var res = canAttackWith(board, card)

        assert.equal(res, true)
      });

      it('2', function () {
        var board = [new CardPair({ attack: new Card({ rank: 6, suit: '♥' }), defence: new Card({ rank: 8, suit: '♥' }) })]
        var card = new Card({ rank: 8, suit: '♣' })
        var res = canAttackWith(board, card)

        assert.equal(res, true)
      });

      it('3', function () {
        var board = [new CardPair({ attack: new Card({ rank: 6, suit: '♥' }), defence: new Card({ rank: 8, suit: '♥' }) }), new CardPair({ attack: new Card({ rank: 6, suit: '♣' }) })]
        var card = new Card({ rank: 7, suit: '♣' })
        var res = canAttackWith(board, card)

        assert.equal(res, false)
      });
    });
  });

  describe('not canDefenceWith', function () {
    it('card with greater rank and other non trump suit', function () {
      var board = [new CardPair({ attack: new Card({ rank: 10, suit: '♣' }) })]
      var card = new Card({ rank: 11, suit: '♥' })
      var res = canDefenceWith(board, card, '♦')

      assert.equal(res, false)
    });
    it('card with same rank and same suit', function () {
      var board = [new CardPair({ attack: new Card({ rank: 10, suit: '♣' }) })]
      var card = new Card({ rank: 10, suit: '♣' })
      var res = canDefenceWith(board, card, '♦')

      assert.equal(res, false)
    });
  });

  describe('canDefenceWith', function () {
    it('card with greater rank and same suit', function () {
      var board = [new CardPair({ attack: new Card({ rank: 6, suit: '♥' }), defence: new Card({ rank: 8, suit: '♥' }) }), new CardPair({ attack: new Card({ rank: 10, suit: '♣' }) })]
      var card = new Card({ rank: 11, suit: '♣' })
      var res = canDefenceWith(board, card, '♣')

      assert.equal(res, true)
    });

    it('card with trump suit of any rank if attacking card is not of trump suit', function () {
      var board = [new CardPair({ attack: new Card({ rank: 6, suit: '♥' }) })]
      var card = new Card({ rank: 6, suit: '♣' })
      var res = canDefenceWith(board, card, '♣')

      assert.equal(res, true)
    });
  });

});
