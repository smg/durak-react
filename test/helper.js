import Card from '../common/models/Card'
import {getRandomInt} from '../common/helpers'

function createCard() {
  var number = getRandomInt(6, 14)
  var suit = '♥♦♣♠'[getRandomInt(0, 3)]
  return new Card({ rank: number, suit: suit })
}
export { createCard }
