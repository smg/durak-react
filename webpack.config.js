var path = require('path')
var webpack = require('webpack')
var NpmInstallPlugin = require('npm-install-webpack-plugin');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'eval',//'cheap-module-eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    'babel-polyfill',
    './client/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new NpmInstallPlugin(),
    new ExtractTextPlugin('app.css', {
      allChunks: true
    })
  ],
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loaders: ['eslint'],
        include: [
          path.resolve(__dirname, "client"),
          path.resolve(__dirname, "common")
        ],
      }
    ],
    loaders: [
      { test: /\.json$/,
        loader: 'json',
        include: path.resolve(__dirname, "common")
      },
      {
        test: /\.js$/,
        loaders: ['react-hot', 'babel-loader'],
        include: [
          path.resolve(__dirname, "client"),
          path.resolve(__dirname, "common")
        ],
        plugins: ['transform-runtime']
      },
      { test: /\.html$/,
        loader: 'html',
        include: path.resolve(__dirname, "client") },
      { test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader', 'postcss-loader'),
        include: path.resolve(__dirname, "client")  },
      { test: /\.scss$/,
        loaders: ['style', 'css', 'sass'],
        include: path.resolve(__dirname, "client")
        //loader: ExtractTextPlugin.extract('style','css','sass')
     }
    ]
  },
  postcss: function () {
    return [autoprefixer];
  }
}
