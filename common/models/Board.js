import {Record,List} from 'immutable'

export default Record({
  cards: new List(), // of cardpairs
  discard: new List(), // of cards
  stock: new List(), // of cards
})
