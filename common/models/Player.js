import {Record, List} from 'immutable'

var Player = Record({ name: '', isAi: false, hand: new List() })
Player.prototype.isWinner = function () {
  return this.hand.size == 0
}
export default Player
