import {Record} from 'immutable'

var CardPair = Record({ attack: null, defence: null })
export default CardPair

function newCardPair(attack, defence= null) {
  return new CardPair({ attack: attack, defence: defence })
}

export { CardPair, newCardPair }
