import {Record} from 'immutable'
var Card = Record({ rank: 1, suit: '♥' })
export default Card

function getSuitSpec(card) {
  switch (card.suit) {
    case '♥':
      return { name: 'HEARTS', color: 'RED' }
    case '♦':
      return { name: 'DIAMONDS', color: 'RED' }
    case '♣':
      return { name: 'CLUBS', color: 'BLACK' }
    case '♠':
      return { name: 'SPADES', color: 'BLACK' }
    default:
      break;
  }
}

const rankNames = [null, null, null, null, null, null,
  'SIX', 'SEVEN', 'EIGHT', 'NINE', 'TEN',
  'JACK', 'QUEEN', 'KING', 'ACE']

function getRankSpec(card) {
  var rank = card.rank
  switch (rank) {
    case 11:
    case 12:
    case 13:
      return { name: rankNames[rank], isFace: true }

    default:
      return { name: rankNames[rank], isFace: false }
  }
}

function getCardSpec(card) {
  var ss = getSuitSpec(card)
  var rs = getRankSpec(card)
  return  {suit: ss.name, color: ss.color, rank: rs.name, isFace: rs.isFace}
}

function newCard(rank, suit) {
  return new Card({ rank: rank, suit: suit });
}
export {getSuitSpec, getRankSpec, getCardSpec, Card, newCard}
