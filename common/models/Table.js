import {Record,List} from 'immutable'

export default Record({
  players: new List(), // of players
  defencer: null, // index
  board: new List(), // of cardpairs
  stock: null, // list of cards
  trump: null, // card
  error: '',
  attackersQueue: new List(),
  whosTurn: null, // index
  defenceTookAtAttacker: null // index
})
