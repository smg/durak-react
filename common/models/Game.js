import {Record,List} from 'immutable'

export default Record({
  defencer: null, // index
  trump: null, // card
  error: '',
  attackersQueue: new List(),
  whosTurn: null, // index
  lastAttacked: undefined,
  defenceTookAtAttacker: null, // index,
  history: new List()
})
