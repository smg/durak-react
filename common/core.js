// no immutable.js here

import Card from './models/Card'
//import CardPair from './models/CardPair'
import _ from 'lodash'

const HAND_SIZE = 6

// полный набор для игры (несколько колод)
function makePack() {
  var cards = []
  for (var s of '♥♦♣♠') {
    for (var r = 6; r <= 14; r++) {
      cards.push(new Card({ rank: r, suit: s }))
    }
  }
  return _.shuffle(cards)
}

/**
 * пополнение карт
 * fn: Called with hand index, expect Boolean - whether can continue.
 */
function draw(handSizes, defencerIndex, goalHandSize, fn) {
  if (!(handSizes instanceof Array))
    throw new TypeError('handSizes')

  if (!handSizes.length)
    throw new Error('handSizes length')

  if (goalHandSize < 1)
    throw new Error('goalHandSize')

  var orderIndexes = getAttackingOrder(handSizes.length, defencerIndex)
  orderIndexes.push(defencerIndex)
  // console.log('order', orderIndexes);

  var canContinue = true
  for (let i = 0; i < orderIndexes.length && canContinue; i++) {
    var handIndex = orderIndexes[i]

    var take = goalHandSize - handSizes[handIndex]
    for (var k = 0; k < take && canContinue; k++) {
      canContinue = fn(handIndex)
    }
  }
}

// TODO when somebody wins
/**
 * Returns array of attackings players indexes in order of turns.
 * Do not adds players after player at whose turn defence took cards.
 * @param stopAt Index of first attacker not included in result.
 * @param attackerIndex Index of attacker if known (by default computed by defencerIndex)
 */
function getAttackingOrder(playersCount, defencerIndex, stopAt, attackerIndex) {
  if (playersCount < 1)
    throw new RangeError('playersCount')
  if (defencerIndex < 0 || defencerIndex > playersCount - 1)
    throw new RangeError('defencerIndex')
  if (stopAt != undefined &&
    (stopAt < 0 || stopAt > playersCount - 1))
    throw new RangeError('stopAt')

  var indexes = []

  // ignore undefined or null
  if (attackerIndex == undefined)
    attackerIndex = getAttackerIndex(defencerIndex, playersCount)
  if (stopAt == undefined)
    stopAt = playersCount * 2

  if (stopAt <= attackerIndex)
    stopAt += playersCount

  for (let i = attackerIndex; i < playersCount + attackerIndex && i < stopAt; i++) {
    if (i % playersCount === defencerIndex)
      continue

    indexes.push(i % playersCount)
  }
  return indexes
}
/**
 * Returns reordered array, such that fist item is attackerIndex, excluding exceptItem.
 * @param {Array} queue
 * @param exceptItem
 * @param attackerIndex
 */
function reorderAttackingQueue(queue, exceptItem, attackerIndex) {
  if (!(queue instanceof Array))
    throw new TypeError('queue')
  if (attackerIndex == null || typeof(attackerIndex) !== 'number')
    throw new TypeError('attackerIndex')

  var indexOfFirst = queue.indexOf(attackerIndex)
  var reordered = queue.slice(indexOfFirst).concat(queue.slice(0, indexOfFirst))
  return _.without(reordered, exceptItem)
}

function getAttackerIndex(defencerIndex, playersCount) {
  if (playersCount < 1)
    throw new RangeError('playersCount')
  if (defencerIndex < 0 || defencerIndex > playersCount - 1)
    throw new RangeError('defencerIndex')

  return (defencerIndex + playersCount - 1) % playersCount
}
/**
 * Returns index of starting player or undefined if nobody has trumpSuit.
 */
function whoAttacks(piles, trumpSuit) {
  if (!(piles instanceof Array))
    throw new TypeError('piles')

  if (!piles.length)
    return undefined

  var min = _(piles.map((p, i) => {
    return {
      pileIndex: i,
      minTrumpNumber: _.min(_.filter(p, c => c.suit == trumpSuit).map(c => c.rank))
    }
  })).minBy(x => x.minTrumpNumber)

  if (min)
    return min.pileIndex
  return undefined
}

/**
 * Returns array of array of pack's content.
 */
function deal(pack, handsCount) {
  if (!(pack instanceof Array))
    throw new TypeError('pack')

  var res = []
  for (var i = 0; i < handsCount; i++) {
    var cards = []
    for (var j = 0; j < HAND_SIZE; j++) {
      cards.push(pack.pop())
    }
    res.push(cards)
  }
  return res
}

function canDefenceWith(board, card, trumpSuit) {
  if (!(board instanceof Array))
    throw new TypeError('board')
  if (!(card instanceof Card))
    throw new TypeError('card')
  if (!(trumpSuit instanceof String || typeof trumpSuit === 'string'))
    throw new TypeError('trumpSuit')

  var attackingPair = _.find(board, pair => pair.defence == null)
  if (attackingPair === undefined)
    throw new Error('no attacking card on board')

  var attacking = attackingPair.attack
  return card.suit === trumpSuit && attacking.suit !== trumpSuit ||
    card.rank > attacking.rank && attacking.suit === card.suit;
}

function canAttackWith(board, card) {
  if (!(board instanceof Array))
    throw new TypeError('board')

  if (board.length === 0)
    return true

  if (board.length >= HAND_SIZE) // это должно быть там же где проверка hand size
    return false

  var ranksOnBoard = _.flatMap(board, pair => pair.defence ? [pair.attack.rank, pair.defence.rank] : pair.attack.rank)
  return _.some(ranksOnBoard, rank => rank === card.rank);
}

export { makePack, draw, deal, whoAttacks, canAttackWith, canDefenceWith,
getAttackerIndex, getAttackingOrder, reorderAttackingQueue }
