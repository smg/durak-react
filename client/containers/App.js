import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as handActions from '../actions/HandActions'
import * as userActions from '../actions/UserActions'
import * as tableActions from '../actions/TableActions'
import Table from '../components/Table'
import Menu from '../components/Menu'
import Log from '../components/Log'

class App extends Component {
  componentWillMount() {

  }
  render() {
    const {beginGame } = this.props.tableActions

    return (
      <div className='app'>
        <Menu
          error={this.props.table.game.error}
          players={this.props.table.players}
          />
        {this.props.table.players.size ?
          <Table
            handActions={this.props.handActions}
            tableActions={this.props.tableActions}
            table = {this.props.table}
            />
          :
          <div>
            <button onClick={() => beginGame(2) }>Start 2</button>
            <button onClick={() => beginGame(3) }>Start 3</button>
            <button onClick={() => beginGame(4) }>Start 4</button>
          </div>
        }
        <Log history={this.props.table.game.history} />
      </div>)
  }
}

function mapStateToProps(state) {
  return {
    table: state.table,
    // board: state.table.board,
    // players: state.table.players,
    // game: state.table.game,
    page: state.page
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handActions: bindActionCreators(handActions, dispatch),
    tableActions: bindActionCreators(tableActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
