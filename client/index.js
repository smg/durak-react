import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import configureStore from './store/configureStore'
import './styles/app.scss'

const store = configureStore()

render(
  <Provider store={store}>
    <div className='container'>
      <App />
    </div>
  </Provider>,
  document.getElementById('root')
)
// todo npm install --save react-addons-pure-render-mixin
// split by modules http://jaysoo.ca/2016/02/28/organizing-redux-application/
