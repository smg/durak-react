export const START_GAME = 'START_GAME'
export const END_GAME = 'END_GAME'
export const START_TURN = 'START_TURN'
export const PASS_TURN = 'PASS_TURN'
export const TOGGLE_TURN = 'TOGGLE_TURN'
export const DRAW = 'DRAW'
export const ERROR = 'ERROR'
export const APPEND_TO_LOG = 'APPEND_TO_LOG'

export const HAND_SIZE = 6
