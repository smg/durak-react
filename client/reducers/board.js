import {
  START_GAME
} from '../constants/Game'
import {
  CLEAR_BOARD, THROW_FROM_STOCK, APPEND_TO_DISCARD
} from '../constants/Board'
import {
  DEFENCE, ATTACK
} from '../constants/Hand'

import { } from '../../common/core'
import Board from '../../common/models/Board'
import {newCardPair} from '../../common/models/CardPair'
import _ from 'lodash' // eslint-disable-line no-unused-vars
import {List, fromJS} from 'immutable'

const initialState = new Board()

function startGame(state, pack) {
  return state.merge({
    cards: new List(),
    stock: fromJS(pack),
    discard: new List()
  })
}

function clearBoard(state) {
  return state.merge({
    cards: new List()
  })
}

function throwFromStock(state, cardsCount) {
  var newSize = Math.max(0, state.stock.size - cardsCount)
  return state.merge({
    stock: state.stock.setSize(newSize)
  })
}

function appendToDiscard(state, cards) {
  return state.merge({
    discard: state.discard.concat(cards)
  });
}

function defence(state, card) {
  // last pair only attacking
  let cards = state.cards.update(
    state.cards.size - 1,
    pair => pair.merge({ defence: card })
  )
  return state.merge({
    cards
  })
}

function attack(state, card) {
  return state.merge({
    cards: state.cards.push(newCardPair(card))
  })
}
export default function board(state = initialState, action) {
  switch (action.type) {

    case START_GAME:
      return startGame(state, action.pack)

    case CLEAR_BOARD:
      return clearBoard(state)

    case THROW_FROM_STOCK:
      return throwFromStock(state, action.cardsCount)

    case APPEND_TO_DISCARD:
      return appendToDiscard(state, action.cards)

    case DEFENCE:
      return defence(state, action.card);

    case ATTACK:
      return attack(state, action.card);

    default:
      return state;
  }
}
