import { combineReducers } from 'redux'
import user from './user'
import board from './board'
import players from './players'
import game from './game'

export default combineReducers({
  table: combineReducers(
    {
      board,
      players,
      game
    }
  ),
  user
})
