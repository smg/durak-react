import {
  START_GAME,
  DRAW,
  HAND_SIZE
} from '../constants/Game'
import {
  ADD_PLAYER
} from '../constants/Players'
import {
  APPEND_TO_HAND, THROW_FROM_HAND
} from '../constants/Hand'
import { draw } from '../../common/core'
import Player from '../../common/models/Player'
import _ from 'lodash' // eslint-disable-line no-unused-vars
import {List} from 'immutable'

const initialState = new List()

function startGame(state) {
  return initialState
}

// пополнение карт в порядке ходов
function ondraw(state, activePlayersHandSizes, defencer, cards) {
  var players = state;
  if (cards.size) {
    draw(activePlayersHandSizes, defencer, HAND_SIZE, i => {
      var popped = cards.last()
      players = players.update(
        i,
        player => player.merge(
          {
            hand: player.hand.push(popped)
          }
        )
      )

      cards = cards.pop();
      return cards.size > 0
    })
  }
  return players
}

function throwFromHand(state, card, playerId) {
  return state.update(
    playerId,
    player => player.merge(
      {
        hand: player.hand.filter(function (c) {
          return c !== card
        })
      }
    )
  )
}

function appendToHand(state, cards, playerId) {
  return state.update(
    playerId,
    player => player.merge(
      {
        hand: player.hand.concat(cards)
      }
    )
  );
}

function addPlayer(state, name, isAi, hand) {
  return state.push(new Player({ name, isAi, hand }))
}

export default function table(state = initialState, action) {
  switch (action.type) {
    case START_GAME:
      return startGame(state)

    case DRAW:
      return ondraw(state, action.activePlayersHandSizes, action.defencer, action.cards)

    case APPEND_TO_HAND:
      return appendToHand(state, action.cards, action.playerId)

    case THROW_FROM_HAND:
      return throwFromHand(state, action.card, action.playerId)

    case ADD_PLAYER:
      return addPlayer(state, action.name, action.isAi, action.hand)

    default:
      return state;
  }
}
