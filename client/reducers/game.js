import {
  START_GAME,
  END_GAME,
  START_TURN,
  PASS_TURN,
  TOGGLE_TURN,
  ERROR,
  APPEND_TO_LOG
} from '../constants/Game'
import {
  TOOK, ATTACK, DEFENCE
} from '../constants/Hand'
import { getAttackingOrder, reorderAttackingQueue } from '../../common/core'
import Game from '../../common/models/Game'
import _ from 'lodash' // eslint-disable-line no-unused-vars
import {List, fromJS} from 'immutable'

const initialState = new Game()

function startGame(state, count, defencer, trump) {
  let attackersQueue = fromJS(getAttackingOrder(count, defencer))

  return state.merge({
    trump,
    defencer,
    attackersQueue,
    whosTurn: attackersQueue.get(0), // MAYBE just A or D, if only one attacker move
    defenceTookAtAttacker: null,
    error: ''
  })
}

function endGame(state, fool) {
  var msg = `Game finished. ${(fool ? `Fool: ${fool.name}` : 'Draw')}`
  return state.merge({
    whosTurn: null,
    defencer: null,
    attackersQueue: new List(),
    defenceTookAtAttacker: null,
    error: msg
  })
}

function took(state) {
  // очередь пока не меняется - атакующий еще может подбросить

  if (state.defenceTookAtAttacker == null) {
    let attacker = state.attackersQueue.get(0)
    return state.merge({
      defenceTookAtAttacker: attacker
    })
  }
  else {
    return state
  }
}

function attack(state) {
  return state.merge({
    lastAttacked: state.attackersQueue.get(0)
  })
}

function defence(state, totalPlayersCount) {
  // очередь снова полная
  let attacker = state.attackersQueue.get(0)
  let attackersQueue = fromJS(getAttackingOrder(totalPlayersCount, state.defencer, null, attacker))
  return state.merge({
    attackersQueue
  })
}

function toggleTurn(state) {
  let whosTurn = (state.defencer == state.whosTurn)
    ? state.attackersQueue.get(0)
    : state.defencer
  return state.merge({ whosTurn })
}
/**
 * When starts attacking new defencer
 */
// TODO when wins
function startTurn(state, totalPlayersCount, activePlayers) {
  let attacker = state.defenceTookAtAttacker != null
    ? (state.defencer + 1) % activePlayers // взял
    : state.defencer  // отбился
  let defencer = (attacker + 1) % activePlayers
  let attackersQueue = fromJS(getAttackingOrder(totalPlayersCount, defencer))
  return state.merge({
    lastAttacked: undefined,
    whosTurn: attacker,
    defencer,
    attackersQueue,
    defenceTookAtAttacker: null
  })
}
/**
 * When not last attacker press Done.
 */
function passTurn(state, totalPlayersCount, activePlayers) {
  if (state.attackersQueue.size < 2)
    throw new Error('can not pass turn to next attacker')

  const whosTurn = state.attackersQueue.get(1)
  const lastAttacker = state.attackersQueue.get(0)
  // 1)
  // const firstSkippingAttacker = state.firstSkippingAttacker == -1
  //   ? undefined // was attack - no skipping attacker
  //   : (state.firstSkippingAttacker || prevAttacker) // no attack - save first or set new

  // 2)
  // не было взятия - очередь вся без последнего атакующего

  // если была защита перед взятием - очередь вся, только без defenceTookAtAttacker
  // сразу взятие - в очередь не попадают начиная с последнего атакующего, на котором была защита,
  // если такой был (потому что он уже сходил, и новых карт не будет)

  // записывать firstSkippingAttacker игрока с последней защитой и пропускать с него
  // var lastAttakcerWithDefence;
  // const stopAt = state.defenceTookAtAttacker == undefined
  //   ? lastAttacker // без взятия
  //   : state.defenceTookAtAttacker == lastAttacker
  //     ? lastAttakcerWithDefence // первое взятие на этой атаке
  //     : state.defenceTookAtAttacker //взятие на атаке другого атакующего

  // 3) all cases are same:
  // def OR def and took - queue filled after defence, new queue is "All except lastAttacker"
  // noattack OR took OR def, took OR def, def, took OR def, took, took - new queue is current queue except lastAttacker
  let attackersQueue = fromJS(reorderAttackingQueue(state.attackersQueue.toJS(), lastAttacker, whosTurn))
  return state.merge({
    whosTurn,
    attackersQueue
  })
}

function error(state, message) {
  return state.merge({
    error: message
  })
}

function appendToLog(state, message) {
  return state.merge({
    history: state.history.push(message)
  })
}

export default function table(state = initialState, action) {
  switch (action.type) {

    case START_GAME:
      return startGame(state, action.count, action.defencer, action.trump)

    case END_GAME:
      return endGame(state, action.fool)

    case TOOK:
      return took(state)

    case ATTACK:
      return attack(state)

    case DEFENCE:
      return defence(state, action.totalPlayersCount)

    case TOGGLE_TURN:
      return toggleTurn(state);

    case START_TURN:
      return startTurn(state, action.totalPlayersCount, action.activePlayers)

    case PASS_TURN:
      return passTurn(state, action.totalPlayersCount, action.activePlayers)

    case ERROR:
      return error(state, action.message)

    case APPEND_TO_LOG:
      return appendToLog(state, action.message)

    default:
      return state;
  }
}
