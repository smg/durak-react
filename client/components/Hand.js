import React, {Component, PropTypes} from 'react';
//import PileModel from '../../common/models/Pile'
//import Card from '../components/Card'
import Pile from '../components/Pile'
import {List} from 'immutable'

class Hand extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log('render Hand');
    const {pile, name, player, move, done, attacker, defencer } = this.props
    let doneText = attacker ? 'Done' : 'Take'
    return (
      <div className={'hand' + (player ? ' player' : '')
        + (attacker ? ' attacker' : '')
        + (defencer ? ' defencer' : '') }>
        <div>{name} - {pile.size}</div>
        <Pile cards={pile} overlapping={!player} move={move} />
        { attacker || defencer
          ? <button onClick={done} className='done-btn' >{doneText}</button>
          : ''
        }
      </div>
    );
  }
}

Hand.propTypes = {
  pile: PropTypes.instanceOf(List).isRequired,
  player: PropTypes.bool, // is player's hand
  attacker: PropTypes.bool, // is player's hand
  defencer: PropTypes.bool, // is player's hand
  move: PropTypes.func.isRequired, // try to put selected card on board
};

export default Hand;
