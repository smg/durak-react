jest.unmock('../User');

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import User from '../User';

describe('components', () => {
  describe('User', () => {
    it('changes the text after click', () => {
      // Render a checkbox with label in the document
      const user = TestUtils.renderIntoDocument(
        <User name={''} error={''} handleLogin={() => { } }/>
      );

      const userNode = ReactDOM.findDOMNode(user);

      expect(userNode.textContent).toEqual('Войти');

      // Simulate a click and verify that it is now On
      TestUtils.Simulate.change(
        TestUtils.findRenderedDOMComponentWithTag(user, 'button')
      );
      expect(userNode.textContent).toContain('user');
      expect(user.name).toContain('user')
    });

  })
})