import React, {Component, PropTypes} from 'react';
//import CardModel from '../../common/models/Card'
import Card from '../components/Card'
import {List} from 'immutable'

class Pile extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(card) {
    if (this.props.move)
      this.props.move(card);
  }

  render() {
    const { cards, overlapping, vertical } = this.props
    // console.log(cards);
    const shiftedClassName = overlapping ? (vertical ? 'shift-top' : 'shift-left') : ''
    return (
      <div className='pile'>
        { cards.map((item, index) =>
          <Card
            className={index > 0 ? shiftedClassName : ''}
            card={item}
            key={index}
            onClick={()=>this.handleClick(item)}
            />) }
      </div>
    );
  }
}

Pile.propTypes = {
  cards: PropTypes.instanceOf(List).isRequired,
  overlapping: PropTypes.bool,
  vertical: PropTypes.bool,
  move: PropTypes.func
};

export default Pile;
