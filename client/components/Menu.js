import React, {Component} from 'react';

export default class componentName extends Component {
  render() {
    const { error, players } = this.props

    return (
      <div className='menu'>
        <div className='players'>{players.size} players</div>
        <div className='error-msg'>{error}</div>
      </div>
    );
  }
}

