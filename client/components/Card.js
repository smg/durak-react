import React, {Component, PropTypes} from 'react';
import CardModel from '../../common/models/Card'
import {getCardSpec} from '../../common/models/Card'

class Card extends Component {
  render() {
    let { card, onClick, className } = this.props
    const cardspec = getCardSpec(card)
    const rankClass = `rank ${cardspec.rank.toLowerCase()}-sign`
    const suitClass = `suit ${cardspec.color.toLowerCase()}`

    return (
      <div className={'card ' + className} onClick={onClick}>
        <div className={rankClass}></div>
        <div className={suitClass}>
          {card.suit}</div>
      </div>
    )
  }
}
Card.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  card: PropTypes.instanceOf(CardModel)
};

export default Card;
