import React, {Component, PropTypes} from 'react';
//import Card from '../components/Card'
import Pile from '../components/Pile'
import {List} from 'immutable'

class Board extends Component {
  render() {
    console.log('render Board');
    let {pairsList} = this.props
    let pileCards = pairsList.map((pair) =>
      pair.defence
        ? List.of(pair.attack, pair.defence)
        : List.of(pair.attack))

    return (
      <div className='board'>
        { pileCards.map((pile, index) => {
          return (
            <Pile
              cards={pile}
              overlapping={true}
              key={index}
              />
          )
        }) }
      </div>
    );
  }
}

Board.propTypes = {
  pairsList: PropTypes.instanceOf(List).isRequired
};

export default Board;
