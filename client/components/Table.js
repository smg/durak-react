import React, {Component} from 'react';
import Hand from './Hand'
import Board from './Board'
import Stock from './Stock'
import Discard from './Discard'
import _ from 'lodash'

class Table extends Component {
  // componentWillReceiveProps(nextProps) {
  //   console.log('table r p');
  // }
  // componentWillUpdate(nextProps, nextState) {
  //   console.log('table w u');
  // }

  render() {
    const { handleMove, handleDone } = this.props.handActions
    const { stock, cards, discard } = this.props.table.board
    const { trump, attackersQueue, defencer } = this.props.table.game
    const { players } = this.props.table

    const attacker = attackersQueue.get(0)
    const playerIndex = players.findKey(x => !x.isAi)
    const handAiClass = 'hands-ai-' + (players.size - 1)
    return (
      // ai moves itself
      <div className='table'>
        <div className={handAiClass}>
          { players.map((item, index) =>
            index != playerIndex ?
              <Hand
                key={index}
                name={item.name}
                pile={players.get(index).hand}
                move={_.partial(handleMove, index) }
                done={_.partial(handleDone, index) }
                attacker={attacker === index}
                defencer={defencer === index}
                />
              : ''
          ) }
        </div>
        <Discard count={discard.size} />
        <Board pairsList={cards}/>
        <Stock trump={trump} count={stock.size}/>
        <Hand
          player={true}
          name={players.get(playerIndex).name}
          pile={players.get(playerIndex).hand}
          move={_.partial(handleMove, playerIndex) }
          done={_.partial(handleDone, playerIndex) }
          attacker={attacker === playerIndex}
          defencer={defencer === playerIndex}
          />
      </div>
    );
  }
}

export default Table;
