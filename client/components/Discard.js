import React, {Component, PropTypes} from 'react';
//import Card from '../components/Card'

class Discard extends Component {
  render() {
    let {count } = this.props
    return (
      <div className='discard'>
        <p>{count}</p>
      </div>
    );
  }
}

Discard.propTypes = {
  count: PropTypes.number
};

export default Discard;
