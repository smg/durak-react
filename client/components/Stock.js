import React, {Component, PropTypes} from 'react';
import Card from '../components/Card'
import CardModel from '../../common/models/Card'

class Stock extends Component {
  render() {
    let {count, trump} = this.props
    return (
      <div className='stock'>
        <Card card={trump} />
        <p>{count} left</p>
      </div>
    );
  }
}

Stock.propTypes = {
  count: PropTypes.number.isRequired,
  trump: PropTypes.instanceOf(CardModel).isRequired,
};

export default Stock;
