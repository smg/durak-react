import {
  ATTACK, DEFENCE, TOOK, THROW_FROM_HAND, APPEND_TO_HAND
} from '../constants/Hand'
import {
  CLEAR_BOARD, THROW_FROM_STOCK, APPEND_TO_DISCARD
} from '../constants/Board'
import {
  ERROR, TOGGLE_TURN, START_TURN, PASS_TURN, DRAW, HAND_SIZE, END_GAME, APPEND_TO_LOG
} from '../constants/Game'
import makeActionCreator from './index'
import { canAttackWith, canDefenceWith } from '../../common/core'
import {List, fromJS} from 'immutable' // eslint-disable-line no-unused-vars

const defenceWithAC = makeActionCreator(DEFENCE, 'card', 'totalPlayersCount')
const appendToHandAC = makeActionCreator(APPEND_TO_HAND, 'cards', 'playerId')
const throwFromHandAC = makeActionCreator(THROW_FROM_HAND, 'card', 'playerId')
const throwFromStockAC = makeActionCreator(THROW_FROM_STOCK, 'cardsCount')
const appendToDiscardAC = makeActionCreator(APPEND_TO_DISCARD, 'cards')
const attackWithAC = makeActionCreator(ATTACK, 'card')

const showErrorAC = makeActionCreator(ERROR, 'message')
const appendToLogAC = makeActionCreator(APPEND_TO_LOG, 'message')

const toggleTurnAC = makeActionCreator(TOGGLE_TURN)
const startTurnAC = makeActionCreator(START_TURN, 'totalPlayersCount', 'activePlayers')
const passTurnAC = makeActionCreator(PASS_TURN, 'totalPlayersCount', 'activePlayers')
const tookAC = makeActionCreator(TOOK, 'totalPlayersCount')
const drawAC = makeActionCreator(DRAW, 'activePlayersHandSizes', 'defencer', 'cards')
const endGameAC = makeActionCreator(END_GAME, 'fool')


/**
 * Перемещение карты игроком.
 */
export function handleMove(playerId, card) {
  return (dispatch, getState) => {
    let game = getState().table.game
    let cards = getState().table.board.cards.toArray()
    let players = getState().table.players
    if (game.whosTurn != playerId)
      dispatch(showErrorAC('It is not your turn, wait other player'))
    else {
      // TODO cannot defence after Took
      if (game.defencer === playerId) {
        if (canDefenceWith(cards, card, game.trump.suit)) {
          dispatch(showErrorAC(''))
          dispatch(defenceWithAC(card, players.size)) // TODO when win
          dispatch(throwFromHandAC(card, playerId))
          dispatch(toggleTurnAC())
        } else {
          dispatch(showErrorAC('Can not defence'))
        }
      }
      else {
        // TODO cannot attack when no cards
        if (canAttackWith(cards, card)) {
          dispatch(showErrorAC(''))
          dispatch(attackWithAC(card))
          dispatch(throwFromHandAC(card, playerId))
          dispatch(toggleTurnAC())
        } else {
          dispatch(showErrorAC('Can not attack'))
        }
      }

    }
  }
}

export function handleDone(playerId) {
  return (dispatch, getState) => {
    let game = getState().table.game
    let board = getState().table.board
    let players = getState().table.players
    if (game.whosTurn != playerId)
      dispatch(showErrorAC('It is not your turn, wait other player'))
    else if (board.cards.size == 0)
      dispatch(showErrorAC('Move any card to the board'))
    else {
      dispatch(showErrorAC(''))
      if (game.defencer === playerId) {
        // take
        // attacker must press done also now
        dispatch(tookAC(players.size, players.size))
        dispatch(toggleTurnAC())
        //dispatch(removeattAC(players.size)) // TODO when win
      }
      else {
        // done
        if (game.attackersQueue.size == 1) { // больше некому атаковать
          console.log('last attacker done');
          // TODO auto when can not attck
          let defencerTookCards = game.defenceTookAtAttacker != null
          var boardCards = board.cards
            .map(pair => new List([pair.attack, pair.defence]))
            .flatten(1) // deep level
            .filter(x => x != null)
          if (defencerTookCards) {
            dispatch(appendToHandAC(boardCards, game.defencer))
            dispatch(appendToLogAC('defencer took cards'))
          }
          else {
            dispatch(appendToDiscardAC(boardCards))
            dispatch(appendToLogAC('defencer now can attack'))
          }
          dispatch({ type: CLEAR_BOARD })

          // players reducer распределяет доступные карты
          // board reducer убирает карты из колоды
          var activePlayersHandSizes = players
            .map((x, i) => i != game.defencer || !defencerTookCards
              ? [x.hand.size, x]
              : [x.hand.size + boardCards.size, x]) // у взявшего уже больше карт
            .filter(x => !x[1].isWinner())
            .map(x => x[0])
            .toArray()
          var cardsCount = activePlayersHandSizes.reduce((a, x) => a + Math.max(0, HAND_SIZE - x), 0)
          var cardsFromStock = board.stock.takeLast(cardsCount)
          dispatch(throwFromStockAC(cardsCount))
          dispatch(drawAC(activePlayersHandSizes, game.defencer, cardsFromStock))

          // если отбившийся остался без карт - он теперь выигравший
          var winners = players.filter(x => x.isWinner()).toArray()
          if (winners.length == players.size - 1) {
            var fool = players.filter(x => !x.isWinner()).first()
            dispatch(endGameAC(fool))
          } else if (winners.length == players.size) {
            dispatch(endGameAC(null))
          } else {
            dispatch(startTurnAC(players.size, players.filter(x => !x.isWinner()).count()))
          }

        }
        else { // следующий
          console.log('not last attacker done');
          dispatch(passTurnAC(players.size, players.filter(x => !x.isWinner()).count()))
        }
      }
    }
  }

}

