import {
  START_GAME
} from '../constants/Game'
import {
  ADD_PLAYER
} from '../constants/Players'
import {
  CLEAR_BOARD
} from '../constants/Board'
import makeActionCreator from './index'
import {makePack, deal, whoAttacks } from '../../common/core'
import {List, fromJS} from 'immutable'

export const startGameAC = makeActionCreator(START_GAME, 'count', 'defencer', 'pack', 'trump')
export const addPlayerAC = makeActionCreator(ADD_PLAYER, 'name', 'isAi', 'hand')

export function beginGame(count) {
  return (dispatch) => {
    if (count < 2 || count > 4)
      throw new Error('count out of range')

    let pack = makePack()

    let hands = deal(pack, count)
    let trump = pack[0] // cards taken form end of stock, trump must be at the beginning
    let attacker = whoAttacks(hands, trump.suit)
    while (attacker === undefined) {
      hands.forEach(h => h.push(pack.pop()))
      attacker = whoAttacks(hands, trump.suit)
    }
    let defencer = (attacker + 1) % count

    dispatch({ type: CLEAR_BOARD })
    dispatch(startGameAC(count, defencer, pack, trump))
    for (var i = 0; i < count - 1; i++) {
      dispatch(addPlayerAC('ai' + i, true, fromJS(hands[i])))
    }
    dispatch(addPlayerAC('user', false, fromJS(hands[count - 1])))
  }
}



