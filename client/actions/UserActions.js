import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE
} from '../constants/User'

function fetchRequest() {
  return {
    type: LOGIN_REQUEST
  }
}

function fetchSuccess(name) {
  return {
    type: LOGIN_SUCCESS,
    payload: name
  }
}

function fetchFailure(ex) {
  return {
    type: LOGIN_FAILURE,
    error: true,
    payload: ex
  }
}

export function handleLogin() {
  return (dispatch) => {
    dispatch(fetchRequest())

    setTimeout(() => {
      if (new Date().getSeconds() % 2 == 0)
        dispatch(fetchSuccess('name'))
      else
        dispatch(fetchFailure(new Error('Ошибка авторизации'))        )
    }, 1000);

    // VK.Auth.login((r) => { // eslint-disable-line no-undef
    //   if (r.session) {
    //     let username = r.session.user.first_name;

    //     dispatch({
    //       type: LOGIN_SUCCESS,
    //       payload: username
    //     })

    //   } else {
    //     dispatch({
    //       type: LOGIN_FAILURE,
    //       error: true,
    //       payload: new Error('Ошибка авторизации')
    //     })
    //   }
    // }, 4); // запрос прав на доступ к photo
  }
}